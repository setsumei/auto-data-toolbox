# AutoMata工具箱

#### 介绍
一个迷你工具仓库。包含所有自己日常用的工具集合。

#### 基础用法
am 命令 参数1 [,[参数2][,...]]

后续会新增新的工具。

默认工具箱携带以下命令：

am list  显示所有可用命令

am update 更新am工具箱管理器

am install   -a安装全部   -i安装某个am命令

am uninstall 卸载某个am命令

